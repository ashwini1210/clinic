//
//  PetsModel.swift
//  AsurionVeterinaryClinic
//
//  Created by Chinchkhede, Ashwini on 2020/06/25.
//  Copyright © 2020 Chinchkhede, Ashwini. All rights reserved.
//

import UIKit

class PetsModel: BaseModel {
    
    public var petList : PetList? = nil
    
    override init(delegate : HttpResponseProtocol){
        super.init(delegate: delegate)
        AppConstants.PET_DAO_ID = 0
    }
    
    /*
     * Method to hit on server for PetInfo data
     */
    func requestForPetInfo(){
        guard let url = URL(string:  AppConstants.PET_URL) else {return}
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let dataResponse = data,
                error == nil else {
                    print(error?.localizedDescription ?? "Response Error")
                    self.delegate.onHttpResponse(isSuccess: false, error: error as AnyObject, anyIdentifier: AppConstants.PET_MODEL)
                    return
            }
            
            do{
                let response = response as? HTTPURLResponse
                if response!.statusCode == 200{
                    // here dataResponse received from a network request
                    self.petList = try JSONDecoder().decode(PetList.self,from: dataResponse)
                    self.delegate.onHttpResponse(isSuccess: true, error: nil, anyIdentifier: AppConstants.PET_MODEL)
                }
            } catch let parsingError {
                print("Error", parsingError)
                self.delegate.onHttpResponse(isSuccess: false, error: parsingError as AnyObject, anyIdentifier: AppConstants.PET_MODEL)
            }
        }
        task.resume()
    }
    
}


//import Foundation
//
//var semaphore = DispatchSemaphore (value: 0)
//
//var request = URLRequest(url: URL(string: "https://8e5a35ce-c1ee-445d-a7ce-edd65cfe9887.mock.pstmn.io/AsurionVeterinaryClinic/config")!,timeoutInterval: Double.infinity)
//request.httpMethod = "GET"
//
//let task = URLSession.shared.dataTask(with: request) { data, response, error in
//  guard let data = data else {
//    print(String(describing: error))
//    return
//  }
//  print(String(data: data, encoding: .utf8)!)
//  semaphore.signal()
//}
//
//task.resume()
//semaphore.wait()
