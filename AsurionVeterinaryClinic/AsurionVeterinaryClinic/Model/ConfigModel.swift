//
//  ConfigModel.swift
//  AsurionVeterinaryClinic
//
//  Created by Chinchkhede, Ashwini on 2020/06/25.
//  Copyright © 2020 Chinchkhede, Ashwini. All rights reserved.
//

import UIKit

class ConfigModel: BaseModel {
    public var configInfo : ConfigInfo? = nil
    
    override init(delegate : HttpResponseProtocol){
        super.init(delegate: delegate)
    }
    
    /*
     * Method to hit on server for Config data
     */
    func requestForConfig(){
        guard let url = URL(string:  AppConstants.CONGIG_URL) else {return}
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let dataResponse = data,
                error == nil else {
                    print(error?.localizedDescription ?? "Response Error")
                    self.delegate.onHttpResponse(isSuccess: false, error: error as AnyObject, anyIdentifier: AppConstants.CONGIG_MODEL)
                    return
            }
            
            do{
                let response = response as? HTTPURLResponse
                if response!.statusCode == 200{
                    // here dataResponse received from a network request
                    self.configInfo = try JSONDecoder().decode(ConfigInfo.self, from: dataResponse)
                    self.delegate.onHttpResponse(isSuccess: true, error: nil, anyIdentifier: AppConstants.CONGIG_MODEL)
                }
            } catch let parsingError {
                print("Error", parsingError)
                self.delegate.onHttpResponse(isSuccess: false, error: parsingError as AnyObject, anyIdentifier: AppConstants.CONGIG_MODEL)
            }
        }
        task.resume()
    }
}
