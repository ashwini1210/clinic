

import UIKit

class BaseModel: NSObject {
    
    //Variable to hold value of HttpResponseProtocol
    public var delegate : HttpResponseProtocol!
    
    override init(){
        super.init()
    }
    
    init(delegate : HttpResponseProtocol, identifier : String? = nil) {
        super.init()
        self.delegate = delegate
        
    }
    
    init(delegate : HttpResponseProtocol) {
        super.init()
        self.delegate = delegate
        
    }
 
}
