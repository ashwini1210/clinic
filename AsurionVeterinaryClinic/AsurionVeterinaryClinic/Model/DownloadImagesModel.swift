//
//  DownloadImagesModel.swift
//  AsurionVeterinaryClinic
//
//  Created by Chinchkhede, Ashwini on 2020/06/25.
//  Copyright © 2020 Chinchkhede, Ashwini. All rights reserved.
//

import UIKit

class DownloadImagesModel: NSObject {
    public var imagedelegate : ImageResponseProtocol!
       
       override init(){
           super.init()
       }
       
       init(delegate : ImageResponseProtocol) {
           super.init()
           self.imagedelegate = delegate
           
       }
       
       func downloadImageAsync(URLString: String, placeHolderImage: UIImage?,identifier : String?,id:String?) {
           
           let imageCache = NSCache<NSString, UIImage>()
           
           if let cachedImage = imageCache.object(forKey: NSString(string: URLString)) {
               imagedelegate.onImageResponse(isSuccess: true, image: cachedImage, error: nil, anyIdentifier: identifier, id: id)
               return
           }
           
           if let url = URL(string: URLString) {
               URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                   
                   if error != nil {
                       print("ERROR LOADING IMAGES FROM URL: \(error!)")
                       DispatchQueue.main.async {
                           self.imagedelegate.onImageResponse(isSuccess: true, image: placeHolderImage, error: error as AnyObject, anyIdentifier: identifier,id:id)
                       }
                       return
                   }
                   DispatchQueue.main.async {
                       if let data = data {
                           if let downloadedImage = UIImage(data: data) {
                               imageCache.setObject(downloadedImage, forKey: NSString(string: URLString))
                               self.imagedelegate.onImageResponse(isSuccess: true, image: downloadedImage, error: nil, anyIdentifier: identifier,id:id)
                           }
                       }
                   }
               }).resume()
           }
       }
}
