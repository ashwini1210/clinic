//
//  PetInfo.swift
//  AsurionVeterinaryClinic
//
//  Created by Chinchkhede, Ashwini on 2020/06/25.
//  Copyright © 2020 Chinchkhede, Ashwini. All rights reserved.
//

import Foundation
struct PetList : Decodable{
    let id: String?
    var pets: [Pets]
}

struct Pets :  Decodable{
    let image_url : String?
    let title : String?
    let content_url : String?
    let date_added : String?
}
