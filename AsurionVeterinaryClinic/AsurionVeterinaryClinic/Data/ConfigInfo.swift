//
//  ConfigInfo.swift
//  AsurionVeterinaryClinic
//
//  Created by Chinchkhede, Ashwini on 2020/06/25.
//  Copyright © 2020 Chinchkhede, Ashwini. All rights reserved.
//

import Foundation
struct ConfigInfo: Decodable {
    let settings: Settings?
}

struct Settings: Decodable {
    let isChatEnabled: Bool?
    let isCallEnabled : Bool?
    let workHours : String?
}
