
import Foundation

class AppConstants{
    
    //URL
    static let APP_URL = "https://58b9b983-3cba-498a-9a5a-2c94d923c0e8.mock.pstmn.io/"
    static let CONGIG_URL = APP_URL+"config"
    static let PET_URL = APP_URL+"pet"
    
    
    //MESSAGES
    static let WITHIN_WORKING_HOUR = "Thank you for getting in touch with us. We’ll get back to you as soon as possible"
    static let OUT_OF_WORKING_HOUR = "Work hours has ended. Please contact us again on the next work day"
    static let TRY_AGAIN_MESSAGE = "Something went Wrong. Try Again Later!"
    static let TITLE = "Error"
    static let INTERNET_MESSAGE = "Internet is Not Available. Try Again Later!"
    static let REFRESH_MESSAGE = "Fetching Data From Server. Wait for Refresh Data"
    
    
    //IDENTIFIER
    static let CONGIG_MODEL = "CONGIG_MODEL"
    static let PET_MODEL = "PET_MODEL"
    static let PET_CELL_IDENTIIFET = "PetTableViewCell"
    
    //CONSTANTS
    static var PET_DAO_ID = 0
    static let OFFICE_TEXT = "Office Hours:"
    static let PLACEHOLDER_IMAGE = "dog.png"
    static let STOTYBOARD_NAME = "PetInfo"
    static let STORYBOARD_IDENTIFIER = "PetInfoViewController"
    static let OK_BUTTON_TEXT = "OK"
    static let CELL_NAME = "PetTableViewCell"
}

