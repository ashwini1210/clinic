//
//  AppUtils.swift
//  AsurionVeterinaryClinic
//
//  Created by Chinchkhede, Ashwini on 2020/06/25.
//  Copyright © 2020 Chinchkhede, Ashwini. All rights reserved.
//

import UIKit

class AppUtils: NSObject {
    
    /*
     * Method to give border and colour to any view
     */
    static func borderView(view: UIView, borderWidth : CGFloat, borderColour: CGColor?){
        if(borderColour != nil){
            view.layer.borderColor = borderColour
            view.layer.borderWidth = borderWidth
        }
    }
    
    /*
     * Method to give shadow and colour to any view
     */
    static func shadowView(view: UIView,shadowColour: CGColor?, shadowRadius:CGFloat){
        view.layer.shadowColor = shadowColour
        view.layer.shadowOpacity = 1
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = shadowRadius
    }
    
    /*
     * Method to apply corner radius to any view
     */
    static func cornerRadius(view: UIView,roundCornerWidth : CGFloat){
        view.layer.cornerRadius = roundCornerWidth
    }
    
    //
    // Convert String to base64
    //
    static func convertImageToBase64(image: UIImage) -> String {
        let imageData = image.pngData()!
        return imageData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
    
    //
    // Convert base64 to String
    //
    static func convertBase64ToImage(imageString: String) -> UIImage {
        let imageData = Data(base64Encoded: imageString, options: Data.Base64DecodingOptions.ignoreUnknownCharacters)!
        return UIImage(data: imageData)!
    }
    
    /*
     * Method to get Date by passing hour and min
     */
    static func getDate(hour:Int, min: Int) -> Date{
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        let date = formatter.date(from: "\(hour):\(min)")!
        return date
    }
    
    static func isWithinWorkingHour(time:String?) -> Bool{
        var isWithinCheckinTime = true
        var startTime_hour : String? = ""
        var startTime_min : String? = ""
        var endTime_hour : String? = ""
        var endTime_min : String? = ""
        
        let strArray = time!.components(separatedBy: " ")
        
        if(strArray.count > 1){
            let startTime = strArray[1]
            let time = startTime.components(separatedBy: ":")
            startTime_hour = time[0]
            startTime_min = time[1]
        }
        if(strArray.count > 3){
            let endTime = strArray[3]
            let time = endTime.components(separatedBy: ":")
            endTime_hour = time[0]
            endTime_min = time[1]
        }
        
        //Get checkin date and time from hour and min
        let checkintime = AppUtils.getDate(hour: Int(startTime_hour!)!, min: Int(startTime_min!)!)
        
        //Get checkout date and time from hour and min
        let checkouttime = AppUtils.getDate(hour: Int((endTime_hour)!)!, min: Int((endTime_min)!)!)
        
        //Get current date and time from current hour and min
        let hour = Calendar.current.component(.hour, from: Date())
        let minutes = Calendar.current.component(.minute, from: Date())
        let currentTime = AppUtils.getDate(hour: hour, min: minutes)
        
        //compare currentTime with checkintime & checkouttime.
        if (currentTime < (checkintime) || currentTime > (checkouttime)) {
            isWithinCheckinTime = false
        }
        
        return isWithinCheckinTime
    }
    
}
