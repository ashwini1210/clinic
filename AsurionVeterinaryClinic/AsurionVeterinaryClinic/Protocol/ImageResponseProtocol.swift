
import Foundation
import UIKit

protocol ImageResponseProtocol {
    
    func onImageResponse(isSuccess:Bool?, image:UIImage?, error: AnyObject?, anyIdentifier:String? ,id : String?)
    
}
