

import Foundation
import UIKit


/*
 *  Protocol method to show  response from server in respective viewcontroller
 */
protocol HttpResponseProtocol {
    
    func onHttpResponse(isSuccess:Bool, error: AnyObject?, anyIdentifier:String? )

}
