//
//  HomeView.swift
//  AsurionVeterinaryClinic
//
//  Created by Chinchkhede, Ashwini on 2020/06/25.
//  Copyright © 2020 Chinchkhede, Ashwini. All rights reserved.
//

import UIKit

class HomeView: UIView {

   public var homeViewController : HomeViewController? = nil
    private var originalLabelViewFrame : CGRect? = nil
    
    init(homeViewController : HomeViewController){
        self.homeViewController = homeViewController
        super.init(frame: self.homeViewController!.view.frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    func setCallButton(isCallEnabled:Bool,isChatEnabled:Bool){
        DispatchQueue.main.async {
            if(isCallEnabled && !isChatEnabled){
                //Display Call button and occupy complete view width
                guard let homeVC = self.homeViewController else {return}
                homeVC.callButton.frame = CGRect(x: homeVC.buttonsView.frame.origin.x + 8, y: homeVC.callButton.frame.origin.y, width: homeVC.buttonsView.frame.size.width - 8 , height: homeVC.callButton.frame.size.height)
                homeVC.chatButton.isHidden = true
                homeVC.callButton.isHidden = false
            }
            else if(isChatEnabled && !isCallEnabled){
                //Display chat button and occupy complete view width
                guard let homeVC = self.homeViewController else {return}
                homeVC.chatButton.frame = CGRect(x: homeVC.buttonsView.frame.origin.x + 8, y: homeVC.chatButton.frame.origin.y, width: homeVC.buttonsView.frame.size.width - 8 , height: homeVC.chatButton.frame.size.height)
                homeVC.callButton.isHidden = true
                homeVC.chatButton.isHidden = false
            }
            else if(!isChatEnabled && !isCallEnabled){
                //Change Label view and table view y coordinate. ALso change height of pet tableview
                guard let homeVC = self.homeViewController else {return}
                homeVC.buttonsView.isHidden = true
                homeVC.labelView.frame.origin.y = homeVC.buttonsView.frame.origin.y
                homeVC.petInfoTableview.frame.origin.y = self.originalLabelViewFrame!.origin.y
                homeVC.petInfoTableview.frame.size.height = homeVC.petInfoTableview.frame.size.height + homeVC.labelView.frame.size.height + 1
            }
        }
    }

}
