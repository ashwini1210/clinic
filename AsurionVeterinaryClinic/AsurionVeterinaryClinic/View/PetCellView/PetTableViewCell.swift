//
//  PetTableViewCell.swift
//  AsurionVeterinaryClinic
//
//  Created by Chinchkhede, Ashwini on 2020/06/25.
//  Copyright © 2020 Chinchkhede, Ashwini. All rights reserved.
//

import UIKit

class PetTableViewCell: UITableViewCell {
    
    @IBOutlet weak var petImage: UIImageView!
    @IBOutlet weak var petName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setDataInCell(homeViewController: HomeViewController,indexPath: IndexPath){
        let petModel = homeViewController.petModel
        if(petModel?.petList?.pets.count != 0){
            let petDao = petModel!.petList?.pets[indexPath.row]
            petName.text = petDao?.title
            petImage.image = homeViewController.petImagesArray[indexPath.row]
        }
        
    }
}
