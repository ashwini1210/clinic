//
//  PetInfoViewController.swift
//  AsurionVeterinaryClinic
//
//  Created by Chinchkhede, Ashwini on 2020/06/25.
//  Copyright © 2020 Chinchkhede, Ashwini. All rights reserved.
//

import UIKit
import WebKit

class PetInfoViewController: BaseViewController,WKNavigationDelegate {
    var pets : Pets?
    @IBOutlet weak var webview: WKWebView!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webview.navigationDelegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
    
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if currentReachabilityStatus == .Offline{
            self.displayAlert(title: "", message: AppConstants.INTERNET_MESSAGE, btn1: AppConstants.OK_BUTTON_TEXT)
        }
        else{
            self.loadData()
        }
    }
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print(error.localizedDescription)
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        loader?.isHidden = true
        loader!.stopAnimating()
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        self.displayAlert(title: AppConstants.TITLE, message: AppConstants.TRY_AGAIN_MESSAGE, btn1: AppConstants.OK_BUTTON_TEXT)
        loader!.isHidden = true
        loader!.stopAnimating()
    }
    
    func loadData(){
        if (pets != nil){
            if let url = URL(string: pets!.content_url!){
                let request = URLRequest(url: url)
                webview.load(request)
            }
        }
    }
    
    @IBAction func onBackButtonClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
