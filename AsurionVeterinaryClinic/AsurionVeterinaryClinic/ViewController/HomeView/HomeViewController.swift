//
//  HomeViewController.swift
//  AsurionVeterinaryClinic
//
//  Created by Chinchkhede, Ashwini on 2020/06/25.
//  Copyright © 2020 Chinchkhede, Ashwini. All rights reserved.
//

import UIKit

class HomeViewController: BaseViewController {
    
    @IBOutlet weak var chatButton: UIButton!
    @IBOutlet weak var callButton: UIButton!
    @IBOutlet weak var officeLabel: UILabel!
    @IBOutlet weak var petInfoTableview: UITableView!
    @IBOutlet weak var buttonsView: UIView!
    @IBOutlet weak var labelView: UIView!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    
    private var homeView : HomeView? = nil
    private var configModel : ConfigModel? = nil
    public var petModel : PetsModel? = nil
    public var petImagesArray = [UIImage]()
    private let reuseIdentifier = AppConstants.PET_CELL_IDENTIIFET
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initViews()
        registerCell()
        initializeModels()
    }
    
    /*
     * Method to Initialize views
     */
    func initViews(){
        homeView = HomeView.init(homeViewController: self)
        loader.startAnimating()
        AppUtils.cornerRadius(view: chatButton, roundCornerWidth: 5)
        AppUtils.cornerRadius(view: callButton, roundCornerWidth: 5)
        AppUtils.borderView(view: officeLabel, borderWidth: 1, borderColour: UIColor.lightGray.cgColor)
    }
    
    
    /*
     * Method to Initialize Models
     */
    func initializeModels(){
        if currentReachabilityStatus != .Offline{
            //Initialize ConfigModel
            configModel = ConfigModel.init(delegate: self)
            configModel!.requestForConfig()
            
            //Initialize PetsModel
            petModel = PetsModel.init(delegate: self)
            petModel!.requestForPetInfo()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if currentReachabilityStatus == .Offline{
            self.displayAlert(title: "", message: AppConstants.INTERNET_MESSAGE, btn1: AppConstants.OK_BUTTON_TEXT)
        }
    }
    
    /*
     * Method to register custom Pet cell
     */
    func registerCell(){
        self.petInfoTableview.register(UINib(nibName: AppConstants.CELL_NAME, bundle: nil), forCellReuseIdentifier: reuseIdentifier)
    }
    
    
    @IBAction func onChatButtonClicked(_ sender: Any) {
        let configInfo = self.configModel!.configInfo
        guard let config = configInfo else {
            return
        }
        if(AppUtils.isWithinWorkingHour(time: config.settings?.workHours)){
            self.displayAlert(title: "", message: AppConstants.WITHIN_WORKING_HOUR, btn1: AppConstants.OK_BUTTON_TEXT)
        }else{
            self.displayAlert(title: "", message: AppConstants.OUT_OF_WORKING_HOUR, btn1: AppConstants.OK_BUTTON_TEXT)
        }
    }
    
    @IBAction func onCallButtonClicked(_ sender: Any) {
        let configDao = self.configModel!.configInfo
        guard let config = configDao else {
            return
        }
        if(AppUtils.isWithinWorkingHour(time: config.settings?.workHours)){
            self.displayAlert(title: "", message: AppConstants.WITHIN_WORKING_HOUR, btn1: AppConstants.OK_BUTTON_TEXT)
        }else{
            self.displayAlert(title: "", message: AppConstants.OUT_OF_WORKING_HOUR, btn1: AppConstants.OK_BUTTON_TEXT)
        }
    }
    
}

extension HomeViewController : UITableViewDelegate, UITableViewDataSource{
    
    /*
     * Table view callback
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let petModel = petModel{
            if(petModel.petList != nil){
                return petModel.petList!.pets.count
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! PetTableViewCell
        if let petModel = petModel{
            if petModel.petList != nil{
                cell.setDataInCell(homeViewController: self, indexPath: indexPath)
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let petlists = self.petModel!.petList else {return}
        let storyBoard: UIStoryboard = UIStoryboard(name: AppConstants.STOTYBOARD_NAME, bundle: nil)
        let petInfoViewController = (storyBoard.instantiateViewController(withIdentifier: AppConstants.STORYBOARD_IDENTIFIER) as! PetInfoViewController)
        petInfoViewController.pets = petlists.pets[indexPath.row]
        self.present(petInfoViewController, animated: true, completion: nil)
    }
}

extension HomeViewController: HttpResponseProtocol{
    func onHttpResponse(isSuccess: Bool, error: AnyObject?, anyIdentifier: String?) {
        DispatchQueue.main.async {
            self.loader.stopAnimating()
        }
        if(isSuccess){
            if(anyIdentifier == AppConstants.CONGIG_MODEL){
                let configDao = self.configModel!.configInfo
                if(homeView != nil){
                    if(configDao != nil){
                        if(configDao!.settings != nil){
                            let isCallEnabled = configDao!.settings!.isCallEnabled
                            let isChatEnabled = configDao!.settings!.isChatEnabled
                            DispatchQueue.main.async {
                                self.officeLabel.text = "\(AppConstants.OFFICE_TEXT) \(configDao!.settings!.workHours!)"
                            }
                            homeView?.setCallButton(isCallEnabled: isCallEnabled!, isChatEnabled: isChatEnabled!)
                        }
                    }
                }
            }
            else if(anyIdentifier == AppConstants.PET_MODEL){
                let petListAray = petModel!.petList!.pets
                for petDao in petListAray{
                    let downloadImages = DownloadImagesModel.init(delegate: self)
                    downloadImages.downloadImageAsync(URLString: petDao.image_url!, placeHolderImage: UIImage(named: AppConstants.PLACEHOLDER_IMAGE)!,identifier: AppConstants.PET_MODEL, id: petDao.title)
                    petImagesArray.append(UIImage(named: AppConstants.PLACEHOLDER_IMAGE)!)
                }
                DispatchQueue.main.async {
                    self.petInfoTableview.reloadData()
                }
            }
        }
        else{
            self.displayAlert(title: "", message: AppConstants.TRY_AGAIN_MESSAGE, btn1: AppConstants.OK_BUTTON_TEXT)
        }
    }
    
}

extension HomeViewController: ImageResponseProtocol{
    func onImageResponse(isSuccess: Bool?, image: UIImage?, error: AnyObject?, anyIdentifier: String?,id:String?) {
        if(isSuccess!){
            if(anyIdentifier == AppConstants.PET_MODEL){
                if(image != nil){
                    if let index = petModel!.petList!.pets.firstIndex(where: { (dict) -> Bool in
                        return dict.title == id // Will found index of matched id
                    }) {
                        petImagesArray[index] = image!
                    }
                }
                petInfoTableview.reloadData()
            }
        }
    }
}
