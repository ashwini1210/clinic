//
//  PetsModelTest.swift
//  AsurionVeterinaryClinicTests
//
//  Created by Chinchkhede, Ashwini on 2020/06/25.
//  Copyright © 2020 Chinchkhede, Ashwini. All rights reserved.
//

import XCTest
@testable import AsurionVeterinaryClinic

class PetsModelTest: XCTestCase {
    
    public var petList : PetList? = nil
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testIfBaseURLStringCorrect() {
        let baseURLString = AppConstants.PET_URL
        let expectedBaseURLString = "https://58b9b983-3cba-498a-9a5a-2c94d923c0e8.mock.pstmn.io/pet"
        XCTAssertEqual(baseURLString, expectedBaseURLString, "Base URL does not match expected URL")
    }
    
    
    func testPetModelData() {
        guard let url = URL(string: AppConstants.PET_URL) else {return}
        let exception = expectation(description: "Server Connection Fail")
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let dataResponse = data,
                error == nil else {
                    print(error?.localizedDescription ?? "Response Error")
                    XCTFail()
                    return
            }
            
            do{
                let response = response as? HTTPURLResponse
                if response!.statusCode == 200{
                    
                    //The `XCTAssertNoThrow` can be used to get extra context about the throw
                    XCTAssertNoThrow(try JSONDecoder().decode(PetList.self, from: dataResponse))
                    
                    // here dataResponse received from a network request
                    self.petList = try JSONDecoder().decode(PetList.self,from: dataResponse)
                    XCTAssertNotNil(self.petList)
                    XCTAssertNotNil(self.petList!.pets)
                    
                    let pets = self.petList!.pets.first
                    XCTAssertEqual(pets!.image_url, "https://upload.wikimedia.org/wikipedia/commons/thumb/0/0b/Cat_poster_1.jpg/1200px-Cat_poster_1.jpg", "Wrong Image URL")
                    XCTAssertEqual(pets!.title, "Cat", "Wrong Title")
                    XCTAssertEqual(pets!.content_url, "https://en.wikipedia.org/wiki/Cat", "Wrong Content URL")
                    XCTAssertEqual(pets!.date_added, "2018-06-02T03:27:38.027Z", "Wrong Date")
                    exception.fulfill()
                }
            } catch let parsingError {
                print("Error", parsingError)
                XCTFail()
            }
        }
        task.resume()
        //wait for some time for the expectation (you can wait here more than 30 sec, depending on the time for the response)
        self.waitForExpectations(timeout: 30, handler: { (error) in
            if let error = error {
                print("Failed : \(error.localizedDescription)")
            }
            
        })
    }
}
