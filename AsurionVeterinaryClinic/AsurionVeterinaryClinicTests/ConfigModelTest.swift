//
//  ConfigModelTest.swift
//  AsurionVeterinaryClinicTests
//
//  Created by Chinchkhede, Ashwini on 2020/06/25.
//  Copyright © 2020 Chinchkhede, Ashwini. All rights reserved.
//

import XCTest
@testable import AsurionVeterinaryClinic

class ConfigModelTest: XCTestCase {
    
    public var configInfo : ConfigInfo? = nil

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testIfBaseURLStringCorrect() {
        let baseURLString = AppConstants.CONGIG_URL
        let expectedBaseURLString = "https://58b9b983-3cba-498a-9a5a-2c94d923c0e8.mock.pstmn.io/config"
        XCTAssertEqual(baseURLString, expectedBaseURLString, "Base URL does not match expected URL")
    }
    
    func testConfigModelData() {
        guard let url = URL(string: AppConstants.CONGIG_URL) else {return}
        let exception = expectation(description: "Server Connection Fail")
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let dataResponse = data,
                error == nil else {
                    print(error?.localizedDescription ?? "Response Error")
                    XCTFail()
                    return
            }
            
            do{
                let response = response as? HTTPURLResponse
                if response!.statusCode == 200{
                    
                    //The `XCTAssertNoThrow` can be used to get extra context about the throw
                    XCTAssertNoThrow(try JSONDecoder().decode(ConfigInfo.self, from: dataResponse))

                    // here dataResponse received from a network request
                    self.configInfo = try JSONDecoder().decode(ConfigInfo.self, from: dataResponse)
                    XCTAssertNotNil(self.configInfo)
                    XCTAssertNotNil(self.configInfo!.settings)
                    XCTAssertEqual(self.configInfo!.settings!.isCallEnabled, true)
                    XCTAssertEqual(self.configInfo!.settings!.isChatEnabled, true)
                    XCTAssertEqual(self.configInfo!.settings!.workHours, "M-F 9:00 - 18:00")
                    exception.fulfill()
                }
            } catch let parsingError {
                print("Error", parsingError)
                XCTFail()
            }
        }
        task.resume()
        //wait for some time for the expectation (you can wait here more than 30 sec, depending on the time for the response)
        self.waitForExpectations(timeout: 30, handler: { (error) in
            if let error = error {
                print("Failed : \(error.localizedDescription)")
            }
            
        })
    }
}
