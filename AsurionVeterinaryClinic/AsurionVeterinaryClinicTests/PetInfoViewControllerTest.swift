//
//  PetInfoViewControllerTest.swift
//  AsurionVeterinaryClinicTests
//
//  Created by Chinchkhede, Ashwini on 2020/06/25.
//  Copyright © 2020 Chinchkhede, Ashwini. All rights reserved.
//

import XCTest
@testable import AsurionVeterinaryClinic

class PetInfoViewControllerTest: XCTestCase {
    
    var controller: PetInfoViewController!
    
    override func setUp() {
        continueAfterFailure = false
        let storyBoard: UIStoryboard = UIStoryboard(name: AppConstants.STOTYBOARD_NAME, bundle: nil)
        controller = (storyBoard.instantiateViewController(withIdentifier: AppConstants.STORYBOARD_IDENTIFIER) as! PetInfoViewController)
        controller.loadViewIfNeeded()
    }
    
    override func tearDown() {
        controller = nil
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testIfWebViewExist(){
        XCTAssertNotNil(controller.webview,"Controller should have a Webview")
    }
}
