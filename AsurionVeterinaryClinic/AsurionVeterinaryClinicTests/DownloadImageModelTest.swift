//
//  DownloadImageModelTest.swift
//  AsurionVeterinaryClinicTests
//
//  Created by Chinchkhede, Ashwini on 2020/06/25.
//  Copyright © 2020 Chinchkhede, Ashwini. All rights reserved.
//

import XCTest
@testable import AsurionVeterinaryClinic

class DownloadImageModelTest: XCTestCase {
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testDownloadImageAsync() {
        
        let URLString = "https://upload.wikimedia.org/wikipedia/commons/thumb/0/0b/Cat_poster_1.jpg/1200px-Cat_poster_1.jpg"
        
        guard let url = URL(string: URLString) else {return}
        let exception = expectation(description: "Server Connection Fail")
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let dataResponse = data,
                error == nil else {
                    print(error?.localizedDescription ?? "Response Error")
                    XCTFail()
                    return
            }
            
            if UIImage(data: dataResponse) != nil {
                exception.fulfill()
            }
            else {
                XCTFail()
            }
            
        }
        task.resume()
        //wait for some time for the expectation (you can wait here more than 30 sec, depending on the time for the response)
        self.waitForExpectations(timeout: 30, handler: { (error) in
            if let error = error {
                print("Failed : \(error.localizedDescription)")
            }
            
        })
    }
    
}
