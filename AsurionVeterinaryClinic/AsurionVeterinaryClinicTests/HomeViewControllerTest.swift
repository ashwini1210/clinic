//
//  HomeViewControllerTest.swift
//  AsurionVeterinaryClinicTests
//
//  Created by Chinchkhede, Ashwini on 2020/06/25.
//  Copyright © 2020 Chinchkhede, Ashwini. All rights reserved.
//

import XCTest
@testable import AsurionVeterinaryClinic

class HomeViewControllerTest: XCTestCase {
    
    var controller: HomeViewController!
    
    override func setUp() {
        continueAfterFailure = false
        controller = UIStoryboard(name: "HomeView", bundle: nil).instantiateInitialViewController() as? HomeViewController
        controller.loadViewIfNeeded()
    }
    
    override func tearDown() {
        controller = nil
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testIfLabelIsConnected() throws {
        _ = try XCTUnwrap(controller!.officeLabel, "Office Label is not connected")
    }
    
    func testIfTableviewViewExist(){
        XCTAssertNotNil(controller.petInfoTableview,"Controller should have a TableView")
    }
    
    func testIfChatButtonHasActionAssigned() {
        
        //Check if Controller has UIButton property
        let chatButton: UIButton = controller!.chatButton
        XCTAssertNotNil(chatButton, "View Controller does not have UIButton property")
        
        // Attempt Access UIButton Actions
        guard let chatButtonActions = chatButton.actions(forTarget: controller, forControlEvent: .touchUpInside) else {
            XCTFail("UIButton does not have actions assigned for Control Event .touchUpInside")
            return
        }
        
        // Assert UIButton has action with a method name
        XCTAssertTrue(chatButtonActions.contains("onChatButtonClicked:"))
        
    }
    
    func testIfCallButtonHasActionAssigned() {
        
        //Check if Controller has UIButton property
        let callButton: UIButton = controller!.callButton
        XCTAssertNotNil(callButton, "View Controller does not have UIButton property")
        
        // Attempt Access UIButton Actions
        guard let callButtonActions = callButton.actions(forTarget: controller, forControlEvent: .touchUpInside) else {
            XCTFail("UIButton does not have actions assigned for Control Event .touchUpInside")
            return
        }
        
        // Assert UIButton has action with a method name
        XCTAssertTrue(callButtonActions.contains("onCallButtonClicked:"))
        
    }
}
